SUFFIX={1000:['KB','MB','GB','TB','PB','EB','ZB','YB'],
        1024:['KiB','MiB','GiB','TiB','PiB','EiB','ZiB','YiB']}

def approixmate_size(size,kilo_is_1024=True):
    if size<0:
        raise ValueError("size cannot be negativ!!")
    multiple=1024 if kilo_is_1024 else 1000
    for suffix in SUFFIX[multiple]:
        size/=multiple
        if size<multiple:
            return '{0:.1f} {1}'.format(size,suffix)
    raise ValueError('number is too large')




if __name__ == '__main__':
    print(approixmate_size(1000000000000000,False))
    print(approixmate_size(100000000000))
